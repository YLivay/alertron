package db

import (
	"time"

	"bitbucket.org/ylivay/alertron/pkg/db/models"
)

// CacheType is the type of resources we cache
type CacheType string

// Possible cache types
const (
	CacheTypeGuild   CacheType = "guild"
	CacheTypeChannel CacheType = "channel"
	CacheTypeUser    CacheType = "user"
)

// DB is the database interface
type DB interface {
	Close() error
	GetChannel(channelID string) (*models.Channel, error)
	CreateChannel(channelID string, guildID string) (*models.Channel, error)
	GetSubscriptions() ([]*models.Subscription, error)
	CreateSubscription(userID, channelID string, matchers []string, alertDelay time.Duration) (*models.Subscription, error)
	DeleteSubscription(userID, channelID string) (bool, error)
	CreatePendingNotifications(channelID, message, exceptUserID string) (int64, error)
	GetPendingNotifications() ([]*models.PendingNotification, error)
	GetCachedResourceNames(resources map[CacheType][]string) (map[CacheType]map[string]string, error)
	CacheResourceNames(resources map[CacheType]map[string]string, dur time.Duration) (int64, error)
	PruneCachedResourceNames() (int64, error)
}
