package models

import "time"

// Channel contains channel information
type Channel struct {
	ID      string
	GuildID string
}

// Subscription keeps track of which channels users want to be alerted on
type Subscription struct {
	ID         int
	UserID     string
	ChannelID  string
	Channel    *Channel
	Matchers   []string
	AlertDelay time.Duration
}

// SubscriptionMatcher is a single pattern to match for a subscription
type SubscriptionMatcher struct {
	ID             int
	SubscriptionID int
	Matcher        string
}

// PendingNotification keeps track of notifications scheduled to go out
type PendingNotification struct {
	ID             int
	SubscriptionID int
	Subscription   *Subscription
	AlertOn        *time.Time
}
