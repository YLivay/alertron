package sqlite3

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	// Loads the sqlite3 driver
	_ "github.com/mattn/go-sqlite3"

	"bitbucket.org/ylivay/alertron/data"
	"bitbucket.org/ylivay/alertron/pkg/db/models"
	"bitbucket.org/ylivay/alertron/pkg/fs"

	DB "bitbucket.org/ylivay/alertron/pkg/db"
)

// SQLiteDB is a sqlite3 implementation of the DB
type SQLiteDB struct {
	conn *sql.DB
}

// NewDB creates a new sqlite3 db instance
func NewDB() (*SQLiteDB, error) {
	path, err := fs.ExecutableDir()
	if err != nil {
		return nil, err
	}

	path, err = filepath.Abs(filepath.Join(path, "..", "sqlite.db"))
	if err != nil {
		return nil, err
	}

	// Make sure the db file is readable
	f, err := os.Open(path)
	if err != nil && !os.IsNotExist(err) {
		return nil, err
	}
	f.Close()

	bytes, err := data.Asset("pkg/db/sqlite3/db.sql")
	if err != nil {
		return nil, err
	}

	conn, err := sql.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	_, err = conn.Exec(string(bytes))
	if err != nil {
		conn.Close()
		return nil, err
	}

	db := &SQLiteDB{conn}
	return db, nil
}

// Close closes the db connection
func (db *SQLiteDB) Close() error {
	return db.conn.Close()
}

// GetChannel gets a channel from the DB
func (db *SQLiteDB) GetChannel(channelID string) (*models.Channel, error) {
	row := db.conn.QueryRow("select id, guild_id from channels where id = ?", channelID)

	var id, guildID string
	err := row.Scan(&id, &guildID)
	if err != nil {
		return nil, err
	}

	channel := &models.Channel{
		ID:      id,
		GuildID: guildID,
	}

	return channel, nil
}

// CreateChannel creates or updates a channel in the DB
func (db *SQLiteDB) CreateChannel(channelID string, guildID string) (*models.Channel, error) {
	query := `
		insert or replace into channels (id, guild_id) values (?, ?)`

	_, err := db.conn.Exec(query, channelID, guildID)
	if err != nil {
		return nil, err
	}

	channel := &models.Channel{
		ID:      channelID,
		GuildID: guildID,
	}

	return channel, nil
}

// GetSubscriptions returns all the subscriptions in the DB
func (db *SQLiteDB) GetSubscriptions() ([]*models.Subscription, error) {
	query := `
		select s.id, s.user_id, s.channel_id, s.alert_delay, c.guild_id, sm.matcher
		from subscriptions s
		inner join channels c on s.channel_id = c.id
		inner join subscription_matchers sm on s.id = sm.subscription_id`

	rows, err := db.conn.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	subscriptions := []*models.Subscription{}
	subscriptionMatchers := map[int][]string{}

	for rows.Next() {
		var id, alertDelay int
		var userID, channelID, guildID, matcher string
		err = rows.Scan(&id, &userID, &channelID, &alertDelay, &guildID, &matcher)
		if err != nil {
			return nil, err
		}

		duration, err := time.ParseDuration(fmt.Sprintf("%ds", alertDelay))
		if err != nil {
			return nil, err
		}

		matchers, ok := subscriptionMatchers[id]
		if !ok {
			matchers = []string{matcher}

			subscriptions = append(subscriptions, &models.Subscription{
				ID:        id,
				UserID:    userID,
				ChannelID: channelID,
				Channel: &models.Channel{
					ID:      channelID,
					GuildID: guildID,
				},
				AlertDelay: duration,
			})
		} else {
			matchers = append(matchers, matcher)
		}
		subscriptionMatchers[id] = matchers
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	for _, subscription := range subscriptions {
		subscription.Matchers = subscriptionMatchers[subscription.ID]
	}

	return subscriptions, nil
}

// CreateSubscription creates a new subscription in the DB
func (db *SQLiteDB) CreateSubscription(userID, channelID string, matchers []string, alertDelay time.Duration) (*models.Subscription, error) {
	tx, err := db.conn.Begin()
	if err != nil {
		return nil, err
	}

	query := `
		insert or replace into subscriptions (user_id, channel_id, alert_delay) values (?, ?, ?)`

	seconds := int(alertDelay.Seconds())
	res, err := tx.Exec(query, userID, channelID, seconds)
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	subID, err := res.LastInsertId()
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	for _, matcher := range matchers {
		query = `
			insert into subscription_matchers (subscription_id, matcher) values (?, ?)`

		_, err := tx.Exec(query, subID, matcher)
		if err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	sub := &models.Subscription{
		UserID:     userID,
		ChannelID:  channelID,
		Matchers:   matchers,
		AlertDelay: alertDelay,
	}

	return sub, nil
}

// DeleteSubscription deletes a subscription from the DB
func (db *SQLiteDB) DeleteSubscription(userID, channelID string) (bool, error) {
	res, err := db.conn.Exec("delete from subscriptions where user_id = ? and channel_id = ?", userID, channelID)
	if err != nil {
		return false, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return false, err
	}

	return rowsAffected > 0, nil
}

// CreatePendingNotifications creates notifications as needed when a message arrives in a channel
func (db *SQLiteDB) CreatePendingNotifications(channelID, message, exceptUserID string) (int64, error) {
	now := time.Now().Unix()

	tx, err := db.conn.Begin()
	if err != nil {
		return 0, err
	}

	query := `
		insert or ignore into pending_notifications (subscription_id, alert_on)
		select s.id, ? + s.alert_delay
		from subscriptions s
		where s.channel_id = ?
		and (
			select sm.id
			from subscription_matchers sm
			where sm.subscription_id = s.id
			and ? like sm.matcher escape '\'
		)`

	res, err := tx.Exec(query, now, channelID, message)
	if err != nil {
		tx.Rollback()
		return 0, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		tx.Rollback()
		return 0, err
	}

	var rowsDeleted int64
	if exceptUserID != "" {
		query = `
			delete from pending_notifications
			where id in (
				select n.id
				from pending_notifications n
				inner join subscriptions s on n.subscription_id = s.id
				where s.user_id = ?
			)`

		res, err = tx.Exec(query, exceptUserID)
		if err != nil {
			tx.Rollback()
			return 0, err
		}
		if err != nil {
			tx.Rollback()
			return 0, err
		}

		rowsDeleted, err = res.RowsAffected()
		if err != nil {
			tx.Rollback()
			return 0, err
		}
	}

	if err := tx.Commit(); err != nil {
		return 0, err
	}

	return rowsAffected - rowsDeleted, nil
}

// GetPendingNotifications returns notifications that should be alerted now
func (db *SQLiteDB) GetPendingNotifications() ([]*models.PendingNotification, error) {
	now := time.Now().Unix()

	query := `
		select n.id as notification_id, n.alert_on, n.subscription_id, s.user_id, s.channel_id, s.alert_delay
		from pending_notifications n
		inner join subscriptions s on n.subscription_id = s.id
		where n.alert_on <= ?`

	rows, err := db.conn.Query(query, now)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	notifications := []*models.PendingNotification{}
	for rows.Next() {
		var notificationID, subscriptionID, alertDelay int
		var alertOn int64
		var userID, channelID string
		err = rows.Scan(&notificationID, &alertOn, &subscriptionID, &userID, &channelID, &alertDelay)
		if err != nil {
			return nil, err
		}

		alertOnTime := time.Unix(alertOn, 0)
		duration, err := time.ParseDuration(fmt.Sprintf("%ds", alertDelay))
		if err != nil {
			return nil, err
		}

		notifications = append(notifications, &models.PendingNotification{
			ID:             notificationID,
			SubscriptionID: subscriptionID,
			Subscription: &models.Subscription{
				UserID:     userID,
				ChannelID:  channelID,
				AlertDelay: duration,
			},
			AlertOn: &alertOnTime,
		})
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	ids := make([]interface{}, len(notifications))
	placeholders := make([]string, len(notifications))
	for i, notification := range notifications {
		ids[i] = notification.ID
		placeholders[i] = "?"
	}

	query = `delete from pending_notifications where id in (` + strings.Join(placeholders, ",") + `)`
	if _, err := db.conn.Exec(query, ids...); err != nil {
		return nil, err
	}

	return notifications, nil
}

// GetCachedResourceNames gets cached resource names from the DB
// resources is a map of resource type => resource ids
// the result is a matching map of resource type => resource names
func (db *SQLiteDB) GetCachedResourceNames(resources map[DB.CacheType][]string) (map[DB.CacheType]map[string]string, error) {
	conditions := []string{}
	params := []interface{}{}
	for resourceType, resourceIDs := range resources {

		// Make placeholder string
		var placeholders string
		params = append(params, resourceType)
		for _, resourceID := range resourceIDs {
			placeholders += ", ?"
			params = append(params, resourceID)
		}

		// Trim the first comma in the placeholder string
		conditions = append(conditions, "(resource_type = ? and resource_id in ("+placeholders[1:]+"))")
	}

	query := `
		select resource_type, resource_id, resource_name
		from cached_resource_names
		where ` + strings.Join(conditions, " or ")

	rows, err := db.conn.Query(query, params...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	results := map[DB.CacheType]map[string]string{}

	for rows.Next() {
		var resourceType DB.CacheType
		var resourceID, resourceName string
		err = rows.Scan(&resourceType, &resourceID, &resourceName)
		if err != nil {
			return nil, err
		}
		m, ok := results[resourceType]
		if !ok {
			m = map[string]string{}
			results[resourceType] = m
		}
		m[resourceID] = resourceName
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return results, nil
}

// CacheResourceNames creaetes eor updates the cached resource names in the DB.
func (db *SQLiteDB) CacheResourceNames(resources map[DB.CacheType]map[string]string, duration time.Duration) (int64, error) {
	now := time.Now().Unix()
	expireOn := now + int64(duration.Seconds())

	values := []string{}
	params := []interface{}{}
	for resourceType, resourceIDs := range resources {
		for resourceID, resourceName := range resourceIDs {
			values = append(values, "(?, ?, ?, ?)")
			params = append(params, resourceType, resourceID, resourceName, expireOn)
		}
	}

	query := `
		insert or replace into cached_resource_names (resource_type, resource_id, resource_name, expire_on)
		values ` + strings.Join(values, ", ")

	res, err := db.conn.Exec(query, params...)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}

// PruneCachedResourceNames prunes the expired cache resource names
func (db *SQLiteDB) PruneCachedResourceNames() (int64, error) {
	res, err := db.conn.Exec("delete from cached_resource_names where expire_on < ?", time.Now().Unix())
	if err != nil {
		return 0, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}
