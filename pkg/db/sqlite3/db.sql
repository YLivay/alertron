PRAGMA foreign_keys = ON;

create table if not exists `channels` (
    `id` varchar(20) primary key,
    `guild_id` varchar(20) not null
);

create table if not exists `subscriptions` (
    `id` integer primary key autoincrement,
    `user_id` varchar(20) not null,
    `channel_id` varchar(20) not null,
    `alert_delay` int unsigned not null
);
create index if not exists `channel_id` on `subscriptions` (`channel_id`);
create unique index if not exists `user_and_channel` on `subscriptions` (`user_id`, `channel_id`);

create table if not exists `subscription_matchers` (
    `id` integer primary key autoincrement,
    `subscription_id` int unsigned not null,
    `matcher` varchar(200) not null,
    foreign key (`subscription_id`)
        references `subscriptions` (`id`)
        on update cascade
        on delete cascade
);

create table if not exists `pending_notifications` (
    `id` integer primary key autoincrement,
    `subscription_id` int unsigned not null,
    `alert_on` int unsigned not null,
    foreign key (`subscription_id`)
        references `subscriptions` (`id`)
        on update cascade
        on delete cascade
);
create index if not exists `alert_on` on `pending_notifications` (`alert_on`);
create unique index if not exists `u_sub` on `pending_notifications` (`subscription_id`);

create table if not exists `cached_resource_names` (
    `id` integer primary key autoincrement,
    `resource_type` varchar(20) not null,
    `resource_id` varchar(20) not null,
    `resource_name` varchar(200) not null,
    `expire_on` int unsigned not null
);
create index if not exists `expire_on` on `cached_resource_names` (`expire_on`);
create unique index if not exists `resource_type_resource_id` on `cached_resource_names` (`resource_type`, `resource_id`);