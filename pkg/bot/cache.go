package bot

import (
	"errors"
	"fmt"
	"reflect"
	"sync"
	"time"

	"bitbucket.org/ylivay/alertron/pkg/broadcast"
	DB "bitbucket.org/ylivay/alertron/pkg/db"
	"bitbucket.org/ylivay/alertron/pkg/ui"
	"github.com/bwmarrin/discordgo"
)

func getInCacheMap(m map[DB.CacheType]map[string]string, resourceType DB.CacheType, resourceID string) (string, bool) {
	m2, ok := m[resourceType]
	if !ok {
		return "", false
	}

	result, ok := m2[resourceID]
	if !ok {
		return "", false
	}

	return result, true
}

func setInCacheMap(m map[DB.CacheType]map[string]string, resourceType DB.CacheType, resourceID, resourceName string) {
	m2, ok := m[resourceType]
	if !ok {
		m2 = map[string]string{}
		m[resourceType] = m2
	}

	m2[resourceID] = resourceName
}

func (bot *Bot) getFromCache(resourceType DB.CacheType, resourceID string, forceLookup bool) (string, bool, error) {
	bot.cacheMu.Lock()
	resourceName, ok := getInCacheMap(bot.cacheMap, resourceType, resourceID)
	bot.cacheMu.Unlock()

	// empty strings in resource names mean we've queried the cache and found no results.
	// we cache the no results found too to avoid querying the db over and over for it.
	if ok && !forceLookup {
		return resourceName, true, nil
	}

	resources := map[DB.CacheType][]string{}
	resources[resourceType] = []string{resourceID}
	cacheToAdd, err := bot.fetchFoSho(resources)
	if err != nil {
		return "", false, err
	}

	resourceName, _ = getInCacheMap(cacheToAdd, resourceType, resourceID)
	bot.cacheMulti(cacheToAdd)

	return resourceName, false, nil
}

func (bot *Bot) fetchFoSho(resources map[DB.CacheType][]string) (map[DB.CacheType]map[string]string, error) {
	cacheToAdd, err := bot.db.GetCachedResourceNames(resources)
	if err != nil {
		return nil, errors.New("Couldn't query cached resource names: " + err.Error())
	}

	notFoundEntries := getNotFoundCacheEntries(resources, cacheToAdd)
	if len(notFoundEntries) > 0 {
		discordEntries := <-bot.getFromDiscord(notFoundEntries)
		for discordType, discordIDs := range discordEntries {
			for discordID, discordFetch := range discordIDs {
				if discordFetch.err == nil {
					setInCacheMap(cacheToAdd, discordType, discordID, discordFetch.Name())
				} else {
					ui.Warn("Discord fetch failed (type: %s, id: %s): %s\n", discordType, discordID, discordFetch.err.Error())
				}
			}
		}
	}

	return cacheToAdd, nil
}

type discordFetchResult struct {
	obj  interface{}
	name *string
	err  error
}

func (d *discordFetchResult) Name() string {
	if d.name != nil {
		return *d.name
	}

	obj := d.obj

	switch obj.(type) {
	case *discordgo.Guild:
		return obj.(*discordgo.Guild).Name
	case *discordgo.Channel:
		return obj.(*discordgo.Channel).Name
	case *discordgo.User:
		return obj.(*discordgo.User).String()
	}

	return ""
}

func getNotFoundCacheEntries(requestCache map[DB.CacheType][]string, cacheMap map[DB.CacheType]map[string]string) map[DB.CacheType][]string {
	result := map[DB.CacheType][]string{}

	for resourceType, resourceIDs := range requestCache {
		var ids []string

		cacheMapResourceNames, ok := cacheMap[resourceType]
		if !ok {
			ids = resourceIDs
		} else {
			ids = []string{}
			for _, resourceID := range resourceIDs {
				resourceName, ok := cacheMapResourceNames[resourceID]
				if !ok || resourceName == "" {
					ids = append(ids, resourceID)
					continue
				}
			}
		}

		if len(ids) > 0 {
			result[resourceType] = ids
		}
	}

	return result
}

func (bot *Bot) getFromDiscord(resources map[DB.CacheType][]string) <-chan map[DB.CacheType]map[string]discordFetchResult {
	ch := make(chan map[DB.CacheType]map[string]discordFetchResult, 1)
	results := map[DB.CacheType]map[string]discordFetchResult{}

	var wg sync.WaitGroup
	for resourceType, resourceIDs := range resources {
		wg.Add(len(resourceIDs))

		results[resourceType] = map[string]discordFetchResult{}
		for _, resourceID := range resourceIDs {
			go func(resourceType DB.CacheType, resourceID string) {
				result := <-bot.getSingleFromDiscord(resourceType, resourceID)
				results[resourceType][resourceID] = result
				wg.Done()
			}(resourceType, resourceID)
		}
	}

	go func() {
		wg.Wait()
		ch <- results
		close(ch)
	}()

	return ch
}

func (bot *Bot) tryJoinExistingGetFromDiscord(resourceType DB.CacheType, resourceID string) <-chan discordFetchResult {
	ch := make(chan discordFetchResult)

	bot.cacheMu.Lock()
	defer bot.cacheMu.Unlock()

	m, ok := bot.cacheDiscordFetches[resourceType]
	if !ok {
		m = map[string]*broadcast.Broadcaster{}
		bot.cacheDiscordFetches[resourceType] = m
	}

	broadcaster, ok := m[resourceID]
	if !ok {
		broadcaster = broadcast.NewBroadcaster()
		m[resourceID] = broadcaster
	}

	if ok {
		sub, err := broadcaster.Join()
		if err != nil {
			if err == broadcast.ErrAlreadyClosed {
				name, hasValue := getInCacheMap(bot.cacheMap, resourceType, resourceID)
				go func() {
					defer close(ch)

					if hasValue {
						ch <- discordFetchResult{name: &name}
					} else {
						ch <- discordFetchResult{err: errors.New("Some race condition while fetching from discord")}
					}
				}()

				return ch
			}

			go func() {
				defer close(ch)
				ch <- discordFetchResult{err: errors.New("Could not subscribe to discord fetch result: " + err.Error())}
			}()

			return ch
		}

		go func() {
			defer sub.Close()
			defer close(ch)

			result, ok := (<-sub.Next()).(discordFetchResult)
			if !ok {
				ch <- discordFetchResult{err: fmt.Errorf("Expected result of discord fetch to be of type discordFetchResult, got: %s", reflect.TypeOf(result))}
			} else {
				ch <- result
			}
		}()

		return ch
	}

	close(ch)
	return nil
}

func (bot *Bot) getSingleFromDiscord(resourceType DB.CacheType, resourceID string) <-chan discordFetchResult {
	ch := make(chan discordFetchResult)

	go func() {
		defer close(ch)

		existingFetch := bot.tryJoinExistingGetFromDiscord(resourceType, resourceID)
		if existingFetch != nil {
			ch <- <-existingFetch
			return
		}

		obj, err := bot.doDiscordFetch(resourceType, resourceID)
		if err != nil {
			ch <- discordFetchResult{err: err}
		} else {
			ch <- discordFetchResult{obj: obj}
		}
	}()

	return ch
}

func (bot *Bot) doDiscordFetch(resourceType DB.CacheType, resourceID string) (interface{}, error) {
	switch resourceType {
	case DB.CacheTypeGuild:
		guild, err := bot.session.Guild(resourceID)
		if err == nil && (guild == nil || guild.Name == "") {
			return "", errors.New("Guild info wasn't fetched")
		}

		return guild, nil

	case DB.CacheTypeChannel:
		channel, err := bot.session.Channel(resourceID)
		if err == nil && (channel == nil || channel.Name == "") {
			return "", errors.New("Channel info wasn't fetched")
		}

		return channel, nil

	case DB.CacheTypeUser:
		var user *discordgo.User
		user, err := bot.session.User(resourceID)
		if err == nil && (user == nil || user.Username == "") {
			return "", errors.New("User info wasn't fetched")
		}

		return user, nil
	}

	return "", errors.New("Unsupported cache type")
}

func (bot *Bot) cache(resourceType DB.CacheType, resourceID, resourceName string) {
	bot.cacheMu.Lock()
	defer bot.cacheMu.Unlock()

	setInCacheMap(bot.cacheMap, resourceType, resourceID, resourceName)
	setInCacheMap(bot.cachePendingSubmit, resourceType, resourceID, resourceName)
}

func (bot *Bot) cacheMulti(cacheMap map[DB.CacheType]map[string]string) {
	bot.cacheMu.Lock()
	defer bot.cacheMu.Unlock()

	for resourceType, resourceIDs := range cacheMap {
		for resourceID, resourceName := range resourceIDs {
			setInCacheMap(bot.cacheMap, resourceType, resourceID, resourceName)
			setInCacheMap(bot.cachePendingSubmit, resourceType, resourceID, resourceName)
		}
	}
}

func (bot *Bot) cacheSubmit() (int64, error) {
	bot.cacheMu.Lock()
	defer bot.cacheMu.Unlock()

	if len(bot.cachePendingSubmit) == 0 {
		return 0, nil
	}

	affected, err := bot.db.CacheResourceNames(bot.cachePendingSubmit, 1*time.Hour)
	bot.cachePendingSubmit = map[DB.CacheType]map[string]string{}
	return affected, err
}

func (bot *Bot) cachePrune() (int64, error) {
	return bot.db.PruneCachedResourceNames()
}
