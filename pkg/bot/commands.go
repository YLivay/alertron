package bot

import (
	"errors"
	"fmt"
	"regexp"

	"bitbucket.org/ylivay/alertron/pkg/ui"
	"github.com/bwmarrin/discordgo"
)

func (bot *Bot) compileCommandPatterns() error {
	var commandPattern string

	// Match 'here' or a channel id
	channelPattern := `(?P<channel>here|\d+)`

	matcherPattern := `(?P<matcher>.*?)`

	// Match duration looking strings like 1s, 30m, 24h
	// Supports composed duration strings like 1h30m10s
	// Supports fractions 1.5m
	durationPattern := `(?P<duration>(?:(?:0|\d+)(?:\.\d+)?[smh]){1,3})`

	// Match alert command with optional channel pattern and duration.
	// alert [channel [[pattern] duration]]
	commandPattern = fmt.Sprintf(`(?P<command>%s)`, regexp.QuoteMeta(bot.config.AlertCommand))
	alertCommandRegex, err := regexp.Compile(fmt.Sprintf(`^%s(?:\s+%s(?:\s+%s(?:\s*%s)?)?)?$`, commandPattern, channelPattern, matcherPattern, durationPattern))
	if err != nil {
		return errors.New("Could not compile command alert pattern matcher: " + err.Error())
	}

	// Match unalert command with optional channel.
	// unalert [channel]
	commandPattern = fmt.Sprintf(`(?P<command>%s)`, regexp.QuoteMeta(bot.config.UnalertCommand))
	unalertCommandRegex, err := regexp.Compile(fmt.Sprintf(`^%s(?:\s+%s)?$`, commandPattern, channelPattern))
	if err != nil {
		return errors.New("Could not compile command unalert pattern matcher: " + err.Error())
	}

	bot.alertCommandRegex = alertCommandRegex
	bot.unalertCommandRegex = unalertCommandRegex

	return nil
}

func regexpMatch(r *regexp.Regexp, s string) map[string]string {
	match := r.FindStringSubmatch(s)
	if match == nil {
		return nil
	}

	captures := map[string]string{}
	for i, name := range r.SubexpNames() {
		if i > 0 && i <= len(match) {
			captures[name] = match[i]
		}
	}

	return captures
}

func (bot *Bot) handleCommands(m *discordgo.MessageCreate) bool {
	if match := regexpMatch(bot.alertCommandRegex, m.Content); match != nil {
		bot.handleAlertCommand(match, m)
		return true
	}

	if match := regexpMatch(bot.unalertCommandRegex, m.Content); match != nil {
		bot.handleUnalertCommand(match, m)
		return true
	}

	return false
}

func (bot *Bot) handleCommandError(m *discordgo.Message, f func() error) {
	if err := f(); err != nil {
		ui.Error("Unexpected error: %s\n\tMessage: %s\n", err.Error(), m.Content)
		if _, err = bot.session.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Sorry %s, something got dun fucked up", m.Author.Mention())); err != nil {
			ui.Error("Failed to send failure message to user. What a disgrace: %s\n", err.Error())
		}
	}
}
