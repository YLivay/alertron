package bot

import (
	"fmt"
	"strings"
	"time"

	DB "bitbucket.org/ylivay/alertron/pkg/db"
	"bitbucket.org/ylivay/alertron/pkg/db/models"
	"bitbucket.org/ylivay/alertron/pkg/ui"
	"github.com/bwmarrin/discordgo"
)

func (bot *Bot) handleAlertCommand(match map[string]string, m *discordgo.MessageCreate) *models.Subscription {
	var result *models.Subscription

	bot.handleCommandError(m.Message, func() error {
		channelID, ok := match["channel"]
		if !ok || channelID == "" || channelID == "here" {
			channelID = m.ChannelID
		}

		var duration time.Duration
		duration = 5 * time.Minute

		if durationStr, ok := match["duration"]; ok && durationStr != "" {
			if dur, err := time.ParseDuration(durationStr); err == nil {
				duration = dur
			}
		}

		var matchers []string
		matcher, ok := match["matcher"]
		if !ok || matcher == "" || matcher == "*" {
			matchers = []string{"%"}
		} else {
			matcherStrs := strings.Split(matcher, ",")
			matchers = make([]string, len(matcherStrs))
			i := 0
			for _, matcher := range matcherStrs {
				matcher = strings.Replace(strings.Replace(strings.Replace(strings.TrimSpace(matcher), "\\", "\\\\", -1), "%", "\\%", -1), "_", "\\_", -1)
				matchers[i] = "%" + matcher + "%"
				i++
			}
		}

		channelName, _, err := bot.getFromCache(DB.CacheTypeChannel, channelID, false)
		if err != nil {
			ui.Warn("Couldn't query channel %s: %s\n", channelID, err.Error())
			_, err := bot.session.ChannelMessageSend(m.ChannelID, fmt.Sprintf("%s, I can't alert you there, i'm not in that channel. Invite me!", m.Author.Mention()))
			if err != nil {
				ui.Error("Could not send !alert failed message to user %s on channel %s: %s", m.Author.String(), m.ChannelID, err.Error())
			}
			return nil
		}

		sub, err := bot.db.CreateSubscription(m.Author.ID, channelID, matchers, duration)
		if err == nil {
			_, err2 := bot.session.ChannelMessageSend(m.ChannelID, fmt.Sprintf("%s, got it! I'll alert you on #%s", m.Author.Mention(), channelName))
			if err2 != nil {
				ui.Error("Could not send !alert confirmation to user %s on channel %s: %s", m.Author.String(), m.ChannelID, err2.Error())
			}
			result = sub
		}
		return err
	})

	return result
}
