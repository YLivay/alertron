package bot

import (
	"errors"
	"fmt"
	"regexp"
	"sync"
	"time"

	"bitbucket.org/ylivay/alertron/pkg/broadcast"
	"bitbucket.org/ylivay/alertron/pkg/config"
	DB "bitbucket.org/ylivay/alertron/pkg/db"
	"bitbucket.org/ylivay/alertron/pkg/db/models"
	"bitbucket.org/ylivay/alertron/pkg/ui"
	dgo "github.com/bwmarrin/discordgo"
)

// Bot is alertron, with all its glorious logic
type Bot struct {
	// core config and connections
	config  *config.Config
	db      DB.DB
	session *dgo.Session

	// cache variables
	cacheMu             *sync.Mutex
	cacheMap            map[DB.CacheType]map[string]string
	cachePendingSubmit  map[DB.CacheType]map[string]string
	cacheDiscordFetches map[DB.CacheType]map[string]*broadcast.Broadcaster
	cacheTicker         *time.Ticker
	cacheTickerStop     chan bool

	// alert variables
	alertTicker         *time.Ticker
	alertTickerStop     chan bool
	alertCommandRegex   *regexp.Regexp
	unalertCommandRegex *regexp.Regexp
}

// NewBot Creates a new alertron bot
func NewBot(config *config.Config, db DB.DB) (*Bot, error) {
	bot := &Bot{
		config: config,
		db:     db,

		cacheMu:             &sync.Mutex{},
		cacheMap:            map[DB.CacheType]map[string]string{},
		cachePendingSubmit:  map[DB.CacheType]map[string]string{},
		cacheDiscordFetches: map[DB.CacheType]map[string]*broadcast.Broadcaster{},
	}

	if err := bot.compileCommandPatterns(); err != nil {
		return nil, err
	}

	session, err := dgo.New("Bot " + config.Token)
	if err != nil {
		return nil, errors.New("Could not create discord sesion: " + err.Error())
	}

	bot.initHandlers(session)

	// Open a websocket connection to Discord and begin listening.
	err = session.Open()
	if err != nil {
		return nil, errors.New("Could not connect to discord websockets: " + err.Error())
	}

	bot.session = session

	bot.initAlertTicker()
	bot.initCacheTicker()

	return bot, nil
}

// Close stops the bot and closes it's discord session
func (bot *Bot) Close() error {
	if bot.alertTickerStop != nil {
		close(bot.alertTickerStop)
	}

	if err := bot.session.Close(); err != nil {
		return errors.New("Could not close discord session: " + err.Error())
	}
	return nil
}

func (bot *Bot) debugStuff() {
	userID := "117605092265426948"
	alertChannelID := "415153305539313684"
	bot.db.CreateSubscription(userID, alertChannelID, []string{"%"}, 1*time.Second)
	channel, _ := bot.session.UserChannelCreate(userID)
	bot.session.ChannelMessageSend(channel.ID, "1")
	bot.session.ChannelMessageSend(channel.ID, "2")
}

// Start initializes the bot
func (bot *Bot) Start() error {
	// bot.debugStuff()

	subs, err := bot.db.GetSubscriptions()
	if err != nil {
		return err
	}

	done := make(chan interface{})
	defer close(done)
	initSubsCh := bot.initSubscription(subs, done)
	err = <-initSubsCh
	if err != nil {
		// log.Println("ERROR handled by bot.Init() - SIGNALING CANCEL")

		// Read until channel is closed to make sure everything's cleaned up
		for range initSubsCh {
		}

		return err
	}

	return nil
}

func outOrDone(out chan<- error, done <-chan interface{}, value error) bool {
	select {
	case out <- value:
		return true
	case <-done:
		return false
	}
}

func (bot *Bot) initSubscription(subs []*models.Subscription, done <-chan interface{}) <-chan error {
	// log.Println("initSubscriptions")
	ch := make(chan error, len(subs))

	var wg sync.WaitGroup
	wg.Add(len(subs))

	clearUserNotifications := func(sub *models.Subscription) {
		if err := <-bot.clearUserNotifications(sub.UserID, done); err != nil {
			ch <- err
		}
		wg.Done()
	}

	for _, sub := range subs {
		go clearUserNotifications(sub)
	}

	go func() {
		wg.Wait()
		// log.Println("DONE initSubscriptions")
		close(ch)
	}()

	return ch
}

func (bot *Bot) clearUserNotifications(userID string, done <-chan interface{}) <-chan error {
	// log.Printf("clearUserNotifications(%s)\n", userID)
	out := make(chan error)

	go func() {
		defer close(out)

		// log.Println("getting user channel for ", userID)
		channel, err := bot.session.UserChannelCreate(userID)
		if err != nil {
			// log.Println("ERROR getting user channel for ", userID, ": ", err.Error())
			outOrDone(out, done, errors.New("Failed to create channel with user"))
			return
		}

		// log.Println("getting messages in channel ", channel.ID, " for user ", userID)
		messages, err := bot.session.ChannelMessages(channel.ID, 0, "", "", "")
		if err != nil {
			// log.Println("ERROR getting messages in channel ", channel.ID, " for user ", userID, ": ", err.Error())
			outOrDone(out, done, errors.New("Failed to get messages"))
			return
		}

		messagesToClear := []*dgo.Message{}
		for _, message := range messages {
			if message.Author.ID == bot.session.State.User.ID {
				messagesToClear = append(messagesToClear, message)
			}
		}

		select {
		case err := <-bot.clearChannelMessages(messagesToClear, done):
			if err != nil {
				// log.Printf("ERROR clearUserNotifications(%s): %s\n", userID, err.Error())
				outOrDone(out, done, err)
				return
			}
			// log.Printf("DONE clearUserNotifications(%s)\n", userID)
		case <-done:
			// log.Printf("DONE clearUserNotifications(%s)\n", userID)
			return
		}
	}()

	return out
}

func (bot *Bot) clearChannelMessages(messages []*dgo.Message, done <-chan interface{}) <-chan error {
	// var channelID string
	// if len(messages) > 0 {
	// 	channelID = messages[0].ChannelID
	// }
	// log.Printf("clearChannelMessages(%s)\n", channelID)

	ch := make(chan error, len(messages))

	var wg sync.WaitGroup
	wg.Add(len(messages))

	clearChannelMessage := func(message *dgo.Message) {
		if err := <-bot.clearChannelMessage(message, done); err != nil {
			ch <- err
		}
		wg.Done()
	}

	for _, message := range messages {
		go clearChannelMessage(message)
	}

	go func() {
		wg.Wait()
		// log.Printf("DONE clearChannelMessages(%s)\n", channelID)
		close(ch)
	}()

	return ch
}

func (bot *Bot) clearChannelMessage(message *dgo.Message, done <-chan interface{}) <-chan error {
	// log.Printf("clearChannelMessage(%s) for channel %s\n", message.ID, message.ChannelID)
	out := make(chan error)

	go func() {
		defer close(out)

		if err := bot.session.ChannelMessageDelete(message.ChannelID, message.ID); err != nil {
			// log.Printf("ERROR clearChannelMessage(%s) for channel %s: %s\n", message.ID, message.ChannelID, err.Error())
			outOrDone(out, done, errors.New("Failed to clean message: "+err.Error()))
			return
		}
		// log.Printf("DONE clearChannelMessage(%s) for channel %s\n", message.ID, message.ChannelID)
	}()

	return out
}

func (bot *Bot) initHandlers(s *dgo.Session) {
	s.AddHandler(bot.onReady)
	s.AddHandler(bot.onGuildUpdate)
	s.AddHandler(bot.onChannelUpdate)
	s.AddHandler(bot.onMessageCreate)
}

func (bot *Bot) onReady(s *dgo.Session, e *dgo.Ready) {
	channelessGuilds := []string{}

	for _, guild := range e.Guilds {
		if guild.Name != "" {
			bot.cache(DB.CacheTypeGuild, guild.ID, guild.Name)
		}

		if guild.Channels == nil || len(guild.Channels) == 0 {
			channelessGuilds = append(channelessGuilds, guild.ID)
		} else {
			for _, channel := range guild.Channels {
				if channel.Name != "" {
					bot.cache(DB.CacheTypeChannel, channel.ID, channel.Name)
				}
			}
		}
	}

	bot.getGuildsChannels(channelessGuilds)
}

func (bot *Bot) getGuildsChannels(guildIDs []string) <-chan error {
	ch := make(chan error, len(guildIDs))

	var wg sync.WaitGroup
	wg.Add(len(guildIDs))

	for _, guildID := range guildIDs {
		go func(_guildID string) {
			if err := <-bot.getGuildChannels(_guildID); err != nil {
				ch <- err
			}
			wg.Done()
		}(guildID)
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	return ch
}

func (bot *Bot) getGuildChannels(guildID string) <-chan error {
	out := make(chan error)

	go func() {
		defer close(out)

		channels, err := bot.session.GuildChannels(guildID)
		if err != nil {
			out <- errors.New("Failed to get guild channels: " + err.Error())
			return
		}

		var wg sync.WaitGroup
		wg.Add(len(channels))

		for _, channel := range channels {
			go func(_channel *dgo.Channel) {
				if err := <-bot.createChannel(_channel); err != nil {
					out <- errors.New("Failed to create channel in DB: " + err.Error())
					return
				}
				wg.Done()
			}(channel)
		}

		wg.Wait()
		out <- nil
	}()

	return out
}

func (bot *Bot) createChannel(channel *dgo.Channel) <-chan error {
	ch := make(chan error)

	go func() {
		_, err := bot.db.CreateChannel(channel.ID, channel.GuildID)
		ch <- err
		close(ch)
	}()

	return ch
}

func (bot *Bot) onGuildUpdate(s *dgo.Session, update *dgo.GuildUpdate) {
	if update.Guild.Name != "" {
		bot.cache(DB.CacheTypeGuild, update.Guild.ID, update.Guild.Name)
	}
}

func (bot *Bot) onChannelUpdate(s *dgo.Session, update *dgo.ChannelUpdate) {
	if update.Channel.Name != "" {
		bot.cache(DB.CacheTypeChannel, update.Channel.ID, update.Channel.Name)
	}
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func (bot *Bot) onMessageCreate(s *dgo.Session, m *dgo.MessageCreate) {
	if bot.session.State.User.ID == m.Author.ID {
		return
	}

	bot.cache(DB.CacheTypeUser, m.Author.ID, m.Author.String())

	if handled := bot.handleCommands(m); handled {
		return
	}

	// In the development channel, don't clear your own notifications by passing an empty author ID
	authorID := m.Author.ID
	if bot.config.TestChannel == m.ChannelID {
		authorID = ""
	}

	_, err := bot.db.CreatePendingNotifications(m.ChannelID, m.Content, authorID)
	if err != nil {
		ui.Error("Failed to create pending notifications for channel %s: %s\n", m.ChannelID, err.Error())
		return
	}
}

func (bot *Bot) initAlertTicker() {
	bot.alertTicker = time.NewTicker(1 * time.Second)
	bot.alertTickerStop = make(chan bool)
	go func() {
		for {
			select {
			case <-bot.alertTicker.C:
				bot.alertTick()
			case <-bot.alertTickerStop:
				return
			}
		}
	}()
}

func (bot *Bot) alertTick() <-chan bool {
	ch := make(chan bool)

	go func() {
		notifications, err := bot.db.GetPendingNotifications()
		if err != nil {
			ui.Error("Failed to do alert tick: %s\n", err.Error())
			return
		}

		var wg sync.WaitGroup
		wg.Add(len(notifications))

		for _, notification := range notifications {
			go func(notification *models.PendingNotification) {
				bot.alertUser(notification)
				wg.Done()
			}(notification)
		}

		go func() {
			wg.Wait()
			close(ch)
		}()
	}()

	return ch
}

func (bot *Bot) alertUser(notification *models.PendingNotification) {
	logError := func(msg string, err error) {
		ui.Error("Failed to alert user:\n\tUser ID: %s\n\tChannel ID: %s\n\tReason: %s: %s\n",
			notification.Subscription.UserID,
			notification.Subscription.ChannelID,
			msg,
			err.Error())
	}

	channel, err := bot.session.UserChannelCreate(notification.Subscription.UserID)
	if err != nil {
		logError("Could not get user channel", err)
		return
	}

	targetChannelID := notification.Subscription.ChannelID
	channelName, _, err := bot.getFromCache(DB.CacheTypeChannel, targetChannelID, false)
	if err != nil {
		ui.Warn("Couldn't query channel %s: %s\n", targetChannelID, err.Error())
		channelName = targetChannelID
	} else if channelName == "" {
		ui.Warn("Channel name returned empty for %s\n", targetChannelID)
		channelName = targetChannelID
	}

	_, err = bot.session.ChannelMessageSend(channel.ID, fmt.Sprintf("You got a message in #%s", channelName))
	if err != nil {
		logError("Could not send message in user channel", err)
		return
	}
}

func (bot *Bot) initCacheTicker() {
	bot.cacheTicker = time.NewTicker(1 * time.Second)
	bot.cacheTickerStop = make(chan bool)
	go func() {
		for {
			select {
			case <-bot.cacheTicker.C:
				bot.cacheTick()
			case <-bot.cacheTickerStop:
				return
			}
		}
	}()
}

func (bot *Bot) cacheTick() <-chan bool {
	ch := make(chan bool)

	go func() {
		defer close(ch)

		pruned, err := bot.cachePrune()
		if err != nil {
			ui.Error("Failed to prune cache: %s\n", err.Error())
		} else if pruned > 0 {
			// ui.Info("Pruned %d cache entries\n", pruned)
		}

		submitted, err := bot.cacheSubmit()
		if err != nil {
			ui.Error("Failed to submit cache entries: %s\n", err.Error())
		} else if submitted > 0 {
			// ui.Info("Submitted %d cache entries\n", submitted)
		}
	}()

	return ch
}
