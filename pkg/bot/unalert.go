package bot

import (
	"fmt"

	"bitbucket.org/ylivay/alertron/pkg/ui"
	"github.com/bwmarrin/discordgo"
)

func (bot *Bot) handleUnalertCommand(match map[string]string, m *discordgo.MessageCreate) {
	bot.handleCommandError(m.Message, func() error {
		var channelID string
		channelID, ok := match["channel"]
		if !ok || channelID == "" || channelID == "here" {
			channelID = m.ChannelID
		}

		deleted, err := bot.db.DeleteSubscription(m.Author.ID, channelID)
		if err == nil {
			var msg string
			if deleted {
				msg = fmt.Sprintf("%s, got it! I'll stop alerting you on this channel", m.Author.Mention())
			} else {
				msg = fmt.Sprintf("%s, you werent alerted on that channel anyways", m.Author.Mention())
			}

			_, err2 := bot.session.ChannelMessageSend(m.ChannelID, msg)
			if err2 != nil {
				ui.Error("Could not send !unalert confirmation to user %s on channel %s: %s", m.Author.String(), m.ChannelID, err2.Error())
			}
		}
		return err
	})
}
