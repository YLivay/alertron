package fs

import (
	"os"
	"path/filepath"
)

// ExecutableDir returns the executable dir
func ExecutableDir() (string, error) {
	path := os.Getenv("ALERTER_DIR")
	if path != "" {
		return path, nil
	}

	path, err := os.Executable()
	if err != nil {
		return "", err
	}

	path, err = filepath.EvalSymlinks(path)
	if err != nil {
		return "", err
	}

	return path, nil
}
