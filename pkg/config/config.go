package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"

	"bitbucket.org/ylivay/alertron/pkg/fs"
)

// Config is the config for the bot
type Config struct {
	Token          string `json:"token"`
	TestChannel    string `json:"testChannel"`
	AlertCommand   string `json:"alertCommand"`
	UnalertCommand string `json:"unalertCommand"`
}

func findConfigFile() (string, error) {
	path := os.Getenv("CONFIG")
	if path != "" {
		return path, nil
	}

	path, err := fs.ExecutableDir()
	if err != nil {
		return path, nil
	}

	path, err = filepath.Abs(filepath.Join(path, "..", "config.json"))
	if err != nil {
		return "", err
	}

	return path, nil
}

// Load read and returns the config file
func Load() (*Config, error) {
	path, err := findConfigFile()
	if err != nil {
		return nil, err
	}

	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	config := &Config{}
	if err := json.Unmarshal(bytes, config); err != nil {
		return nil, err
	}

	return config, nil
}
