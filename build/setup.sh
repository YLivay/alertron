#!/bin/bash
# execute from project root
go get -u github.com/jteeuwen/go-bindata/...
go get ./...

# ignore future changes to config.json so you can override it with your own key
# to undo this: git update-index --no-assume-unchanged config.json
git update-index --assume-unchanged config.json
