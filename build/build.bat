@echo off
rem execute from protect root
go-bindata -o ./data/bindata.go -pkg data ./pkg/db/sqlite3/db.sql
go build -ldflags "-s -w" -o ./alertron.exe ./cmd/alertron