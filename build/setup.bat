@echo off
rem execute from project root
go get -u github.com/jteeuwen/go-bindata/...
go get ./...

rem ignore future changes to config.json so you can override it with your own key
rem to undo this: git update-index --no-assume-unchanged config.json
git update-index --assume-unchanged config.json
