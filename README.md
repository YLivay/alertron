
# Alertron - the friendly creeper bot #

Alertron helps you keep track of what's really important, PMing you when discussion you care about happens.

## Commands ##
In any server Alertron is in, use `!alert`, `!unalert` and `!alerts` to subscribe to conversations.

- `!alert channel pattern delay` - __subscribe to a channel__  
All arguments are _optional_. By default this will subscribe you to all messages in the channel you used the command with a delay of 30 seconds before PMing[1]

    You can get fancy by using the available arguments:

    - __channel__ - Channel ID or `here` for the current channel.
    - __pattern__ - Comma separated list of matches to alert you on, or `*` for all messages.
    - __delay__ - Time to delay the alert of the format `1h2m3s`. For example: `5m30s` would alert you after 5 minutes and 30 seconds.

- `!unalert channel` - __unsubscribe to a channel__  
The `channel` argument is optional. By default this will unsubscribe you from the channel you used the command in, but you can specify the channel to select a different one:
    - __channel__ - Channel ID or `here` for the current channel.

- `!alerts channel` - __lists your alerts__  
The `channel` argument is optional. By default this will  list all of your alerts and their `patterns` (see `!alert`) in the channel you sent the command in.
    - __channel__ - Channel ID, `here` or `all` to list for a specific channel, the current channel or all channels, respectively.

[1] The delay is to avoid spamming you with PMs while you're active in that channel.

## How do I invite Alertron? ##
Discord bots are invited through special invite links.
When you log in to discord and visit that link, you'll see a list of servers you own and a list of permissions Alertron needs to function.

__The permissions Alertron needs are:__

1. __Read messages__
2. __Read messages history__ - for when Alertron is rebooted and needs to catch up on messages he missed
3. __Send messages__ - to confirm your commands were accepted.

Are you the server owner? Click [here](https://discordapp.com/api/oauth2/authorize?client_id=415137426772721664&permissions=68608&scope=bot)!  
Not the server owner? Bug them into inviting! Here's the link:  
https://discordapp.com/api/oauth2/authorize?client_id=415137426772721664&permissions=68608&scope=bot

## I want to host Alertron privately ##

Sure, Alertron tries being a good creeper that requires minimal configuration.

Alertron is written in Go, and works on Windows, Mac and Linux.  

__Build:__

1. Clone the repo
2. Get [Go 1.9 or older](https://golang.org/dl/)
3. Edit the `config.json` file and add your bot token. It is no longer Alertron ;-;
4. Run `build/setup` script to install the project dependencies.
5. Run `build/build` script - it'll create the `alertron` binary in the project root.  
\* You can rename the binary to whatever you want, or edit the build script.

__Run:__ by simply executing the binary. No arguments needed.