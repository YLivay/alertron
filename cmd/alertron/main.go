package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/ylivay/alertron/pkg/bot"
	"bitbucket.org/ylivay/alertron/pkg/config"
	"bitbucket.org/ylivay/alertron/pkg/db"
	"bitbucket.org/ylivay/alertron/pkg/db/sqlite3"
	"bitbucket.org/ylivay/alertron/pkg/ui"
)

func main() {
	ui.Begin()
	defer ui.End()

	config := initConfig()
	db := initDB()
	defer db.Close()

	bot, err := bot.NewBot(config, db)
	if err != nil {
		ui.Error("%s\n", err.Error())
		os.Exit(1)
	}
	defer bot.Close()

	if err = bot.Start(); err != nil {
		ui.Error("%s\n", err.Error())
		os.Exit(1)
	}

	// Wait here until CTRL-C or other term signal is received.
	ui.Success("Alertron is running. Press CTRL-C to exit.\n")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

func initConfig() *config.Config {
	config, err := config.Load()
	if err != nil {
		fmt.Println("Could not load the config: ", err.Error())
		os.Exit(1)
	}

	return config
}

func initDB() db.DB {
	db, err := sqlite3.NewDB()
	if err != nil {
		fmt.Println("Failed to get DB: ", err.Error())
		os.Exit(1)
	}

	return db
}
